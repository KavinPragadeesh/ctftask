import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-body',
  templateUrl: './body.component.html',
  styleUrls: ['./body.component.css']
})
export class BodyComponent implements OnInit {

  x : boolean;
  
  constructor() { }

  scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
      document.getElementById("myBtn").style.display = "block";
    } else {
      document.getElementById("myBtn").style.display = "none";
    }
  }
  
    scroll(){
      console.log("Opened aka ...");
      document.body.scrollTop = 0;
      document.documentElement.scrollTop = 0;
    }
  
  
    ngOnInit() {
  
      this.x = (window.screen.width > 430);
      
      window.addEventListener('scroll', this.scrollFunction, true);
  
    }
  
  }
  
