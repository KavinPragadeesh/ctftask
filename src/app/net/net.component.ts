import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-net',
  templateUrl: './net.component.html',
  styleUrls: ['./net.component.css']
})
export class NetComponent implements OnInit {

  x : boolean;

  constructor() { }

  ngOnInit() {
    this.x = (window.screen.width > 450);

  }

}
