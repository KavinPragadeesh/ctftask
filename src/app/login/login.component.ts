import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  x : boolean;

  constructor() { }

  ngOnInit() {
    this.x = (window.screen.width > 450);
    console.log(this.x);
  }

}
