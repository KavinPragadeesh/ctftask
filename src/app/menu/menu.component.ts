import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  x : boolean;

  constructor() { }

  myFunction() {
    document.getElementById("myDropdown").classList.toggle("show");
  }

  ngOnInit() {
    this.x = (window.screen.width > 450);
    console.log(this.x);
  }

}
