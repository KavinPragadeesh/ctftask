import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  x : boolean;

  constructor() { }

  ngOnInit() {
    this.x = (window.screen.width > 450);
  }

}
