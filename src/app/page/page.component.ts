import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.css']
})
export class PageComponent implements OnInit {

  x : boolean;
  
  constructor() { }

  ngOnInit() {
    this.x = (window.screen.width > 450);
  }

}
