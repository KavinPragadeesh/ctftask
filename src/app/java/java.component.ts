import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-java',
  templateUrl: './java.component.html',
  styleUrls: ['./java.component.css']
})
export class JavaComponent implements OnInit {
  x : boolean;
  constructor() { }

  ngOnInit() {
    this.x = (window.screen.width > 450);
  }

}
