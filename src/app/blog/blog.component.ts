import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.css']
})
export class BlogComponent implements OnInit {


  x : boolean;
  
  constructor() { }

  ngOnInit() {
    this.x = (window.screen.width > 450);
  }

}
