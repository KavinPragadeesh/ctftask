import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-python',
  templateUrl: './python.component.html',
  styleUrls: ['./python.component.css']
})
export class PythonComponent implements OnInit {
  x : boolean;
  constructor() { }

  ngOnInit() {
    this.x = (window.screen.width > 450);
  }

}
